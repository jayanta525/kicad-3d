# KiCad 3D Models

Collection of 3D models for KiCad EDA Projects

- 2.4in TFT
- 433Mhz Helix Antenna
- 


#### Disclaimer: All 3D models were downloaded from [grabcad.com](https://grabcad.com/) and [3dcontentcentral.com](https://www.3dcontentcentral.com/). I don't own, nor have any copyrights to these models.
