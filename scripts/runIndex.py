#write a python file, that copies all the names of files in the 
#models directory and inserts them in a format, 
#    <div class="online_3d_viewer" style="width: 800px; height: 600px;" model="../693063010911.stp">
#      </div>

import os
import sys
import re

#path to the models directory
path = "models"
#path to the index.html file
index = "view/index.html"

#open the index.html file
indexFile = open(index, "r")
#read the file
indexFileContent = indexFile.read()
#close the file
indexFile.close()

#open the models directory
models = os.listdir(path)
#sort the models
models.sort()

#loop through the models
for model in models:
    #create the html code for the model
    html = '<div class="online_3d_viewer" style="width: 800px; height: 600px;" model="./' + model + '"></div>'
    #if the model is not in the index.html file
    if html not in indexFileContent:
        #add the model to the index.html file
        indexFileContent = indexFileContent.replace("</body>", html + "\n</body>")
        #open the index.html file
        indexFile = open(index, "w")
        #write the index.html file
        indexFile.write(indexFileContent)
        #close the index.html file
        indexFile.close()
            
